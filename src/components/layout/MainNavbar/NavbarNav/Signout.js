import React from "react";
import { Nav } from "shards-react";
import SignOut from "aws-amplify-react";


export default() => (

  <Nav navbar className="border-left flex-row">
    <SignOut button-text="Custom Text"></SignOut>
  </Nav>
);
