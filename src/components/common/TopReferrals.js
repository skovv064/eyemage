import React from "react";
import PropTypes from "prop-types";
import {
  Card,
  CardHeader,
  CardBody,
  ListGroup,
  ListGroupItem
} from "shards-react";

const TopReferrals = ({ title, referralData }) => (
  <Card small>
    <CardHeader className="border-bottom">
      <h6 className="m-0">{title}</h6>
      <div className="block-handle" />
    </CardHeader>

    <CardBody className="p-0">
      <ListGroup small flush className="list-group-small">
        {referralData.map((item, idx) => (
          <ListGroupItem key={idx} className="d-flex px-3">
            <span className="text-semibold text-fiord-blue">{item.title}</span>
            <span className="ml-auto text-right text-semibold text-reagent-gray">
              {item.value}
            </span>
          </ListGroupItem>
        ))}
      </ListGroup>
    </CardBody>
  </Card>
);

TopReferrals.propTypes = {
  /**
   * The component's title.
   */
  title: PropTypes.string,
  /**
   * The referral data.
   */
  referralData: PropTypes.array
};

TopReferrals.defaultProps = {
  title: "FEDS Real-time updates:",
  referralData: [
    {
      title: "Happy",
      value: "10:30AM"
    },
    {
      title: "Sad",
      value: "11:30AM"
    },
    {
      title: "Srikar Kovvali",
      value: "9:30AM"
    },
    {
      title: "Fearful",
      value: "10:30AM"
    },
    {
      title: "Happy",
      value: "7:30AM"
    },
    {
      title: "Surprised",
      value: "6:30AM"
    },
    {
      title: "Happy",
      value: "9:30AM"
    },
    {
      title: "Sad",
      value: "1:30AM"
    }
  ]
};

export default TopReferrals;
