//Import Statements
import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { withAuthenticator } from '@aws-amplify/ui-react'

import routes from "./routes";
import withTracker from "./withTracker";

import "bootstrap/dist/css/bootstrap.min.css";
import "./eyemage-dashboard/styles/shards-dashboards.1.1.0.min.css";



//Create App.js package to be exported and used in application
export default () => (
  <Router basename={process.env.REACT_APP_BASENAME || ""}>
    <div>
      {routes.map((route, index) => {
        return (
          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            component={withTracker(props => {
              return (
                <route.layout {...props}>
                  <route.component {...props} />
                </route.layout>
              );
            })}
          />
        );
      })}
    </div>
  </Router>
)
withAuthenticator();
