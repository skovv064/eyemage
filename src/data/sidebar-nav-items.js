export default function() {
  return [
    {
      title: "Login",
      to: "/login",
      htmlBefore: '<i class="material-icons">edit</i>',
      htmlAfter: ""
    },
    {
      title: "Eyemage Dashboard",
      to: "/eyemage",
      htmlBefore: '<i class="material-icons">edit</i>',
      htmlAfter: ""
    },

  ];
}
